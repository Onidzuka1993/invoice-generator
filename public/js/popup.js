'use strict';

$(function () {
    initDatePicker('.start-date');
    initDatePicker('.end-date');

    let $form = $('.form');
    let $submitButton = $('.btn-primary');

    $form.validate({
        errorPlacement: function(error, element) {
            error.appendTo( element.parent("div").next('div') );
        }
    });

    $submitButton.click(function (e) {
        e.preventDefault();

        if($form.valid()) {

            let startDate = $form.serializeArray()[0].value;
            let endDate = $form.serializeArray()[1].value;

            chrome.runtime.sendMessage({
                type: "threads",
                data: {startDate: startDate, endDate: endDate}
            }, function (response) {
                console.log(response)
            })
        }
    });

    function initDatePicker(target) {
        $(target).datepicker({
            uiLibrary: 'bootstrap4',
            format: 'yyyy-mm-dd'
        });
    }
});