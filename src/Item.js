import PropTypes from "prop-types"
import React, {Component} from 'react';
import ContentEditable from 'react-contenteditable'
import {Draggable} from 'react-beautiful-dnd';
import "./css/Item.css"

const propTypes = {
    id: PropTypes.string.isRequired,
    subject: PropTypes.string.isRequired,
    index: PropTypes.number.isRequired,
    onEditClick: PropTypes.func.isRequired,
    onDeleteClick: PropTypes.func.isRequired
};

class Item extends Component {
    handleChange = (event) => {
        this.props.onEditClick(this.props.id, event.target.value)
    };

    handleDelete = () => {
        this.props.onDeleteClick(this.props.id)
    };

    render() {
        return (
            <Draggable key={this.props.id} draggableId={this.props.id} index={this.props.index}>
                {(provided) => (
                    <li
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                        className="list-group-item d-flex align-items-center py-0"
                    >
                        <i className="mr-3">
                            <img src={ require('./images/Drag-icon.png') } alt="" width="15"/>
                        </i>

                        <ContentEditable
                            html={this.props.subject}
                            tagName="span"
                            className="flex-grow-1 editable py-3 my-1 font-weight-light"
                            disabled={false}
                            onChange={this.handleChange}
                        />

                        <button
                            type="button"
                            className="btn btn-link btn-sm py-3 color-blue font-weight-light"
                            onClick={this.handleDelete}
                        >
                            Remove
                        </button>
                    </li>
                )}
            </Draggable>
        )
    }
}

Item.propTypes = propTypes;

export default Item;
