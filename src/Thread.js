import PropTypes from "prop-types"
import React, {Component} from 'react';

import {CopyToClipboard} from 'react-copy-to-clipboard';
import {Collapse} from "react-collapse"

import "./css/Thread.css"

const propTypes = {
    id: PropTypes.string.isRequired,
    number: PropTypes.number.isRequired,
    thread: PropTypes.object.isRequired,
    onAddClick: PropTypes.func.isRequired,
    onDeleteClick: PropTypes.func.isRequired,
    showAddButton: PropTypes.bool.isRequired
};

class Thread extends Component {
    state = {
        isOpened: false
    };

    handleAddToItems = () => {
        this.props.onAddClick(this.props.id)
    };

    handleRemoveThread = () => {
        this.props.onDeleteClick(this.props.id)
    };

    handleOpenClose = (event) => {
        event.preventDefault();

        if (this.state.isOpened) {
            this.setState({isOpened: false})
        } else {
            this.setState({isOpened: true})
        }
    };

    showAddButton = () => {
        if (this.props.showAddButton)
            return (
                <button
                    onClick={this.handleAddToItems}
                    className="btn btn-sm mr-2 background-blue color-white shadow-sm"
                >
                    <i className="fas fa-plus">
                    </i>
                </button>
            )
    };

    showRemoveButton = () => {
        return(
            <button
                onClick={this.handleRemoveThread}
                className="btn btn-sm mr-3 mr-4 shadow-sm"
            >
                <i className="fas fa-minus color-blue">
                </i>
            </button>
        )
    };

    showCreatedAt = () => {
        let date = new Date(Number.parseInt(this.props.thread.createdAt));
        return date.toLocaleDateString("en-US", {month: 'short', day: 'numeric'});
    };

    decodeHTML = (value) => {
        let text = document.createElement('textarea');
        text.innerHTML = value;
        return text.value;
    };

    isSelected = () => {
        if (!this.props.showAddButton)
            return 'selected';
        else {
            return '';
        }
    };

    render() {
        return (
            <li
                className={'list-group-item d-flex justify-content-between align-items-center position-relative border-0 pl-0 ' + this.isSelected()}
            >
                <div className="flex-grow-1">
                    <a
                        href=""
                        className="thread w-100 d-inline-block"
                        style={{'minHeight': '32px'}}
                        onClick={this.handleOpenClose}
                    >
                        <small className="mr-2 created-at">{ this.showCreatedAt(this.props.thread.createdAt) }</small>
                        <b className="font-weight-light subject">{this.props.thread.subject}</b>
                    </a>
                    <Collapse isOpened={this.state.isOpened} style={{color: '#535353'}}>
                        <div className="position-relative">
                            <CopyToClipboard
                                text={this.props.thread.snippet}
                            >
                                <span className="fas fa-copy copy-button"> </span>
                            </CopyToClipboard>
                            <small><span className="font-weight-light">From:</span> <span className="font-weight-bold">{this.decodeHTML(this.props.thread.from)}</span></small>
                        </div>
                        <div>
                            <small><span className="font-weight-light">To:</span> <span className="font-weight-bold">{this.decodeHTML(this.props.thread.to)}</span></small>
                        </div>
                        <div>
                            <small className="font-weight-light">{this.decodeHTML(this.props.thread.snippet)}</small>
                        </div>
                    </Collapse>
                </div>

                {this.showAddButton()}
                {this.showRemoveButton()}
            </li>
        )
    }
}

Thread.propTypes = propTypes;

export default Thread;
