import PropTypes from "prop-types"
import React, {Component} from 'react';
import Thread from "./Thread";
import { PRODUCT_LINK } from "./constants/Constants";

import './css/ThreadsList.css'

const propTypes = {
    trialEnded: PropTypes.bool.isRequired,
    threads: PropTypes.array.isRequired,
    onAddClick: PropTypes.func.isRequired,
    existsInItems: PropTypes.func.isRequired,
    onDeleteClick: PropTypes.func.isRequired
};

class ThreadsList extends Component {
    showThreadsCount = (threads) => {
        return <h6 className="mb-2 font-weight-light total-messages" style={{color: '#535353'}}>Showing {threads.length} messages</h6>
    };

    trialMessage  = () => {
        return <h5 className="mt-5 font-weight-bold">Your 14 day trial has expired. <a href={PRODUCT_LINK} target="_blank">Click here</a> to purchase.</h5>
    };

    render() {
        let threads = this.props.threads.map((thread, index) => (
            <Thread
                key={thread.id}
                id={thread.id}
                number={index + 1}
                thread={thread}
                onAddClick={this.props.onAddClick}
                onDeleteClick={this.props.onDeleteClick}
                showAddButton={!this.props.existsInItems(thread.id)}
            />
        ));

        return(<div>
            {this.showThreadsCount(threads)}
            {this.props.trialEnded ? this.trialMessage() : threads}
        </div>)
    }
}

ThreadsList.propTypes = propTypes;

export default ThreadsList;
