/*global chrome*/

import Utilities from './Utilities'
import {
    GMAIL_API_URL,
    CWS_LICENSE_API_URL,
    TRIAL_PERIOD_DAYS
} from './constants/Constants'

function getThreads(success, failure) {
    let urlParams = new URLSearchParams(window.location.search);

    chrome.storage.local.get(['dates'], function (result) {
        chrome.identity.getAuthToken({interactive: true}, function (token) {

        return fetch(CWS_LICENSE_API_URL + chrome.runtime.id, getRequestOptions(token))
            .then(checkStatus)
            .then(parseJSON)
            .then(function (response) {
                if (urlParams.has('test') || (response.result && response.accessLevel === "FULL")) {
                    fetchThreads(result, token, success);
                } else if (response.result && response.accessLevel === "FREE_TRIAL") {
                    let daysAgoLicenseIssued = Date.now() - parseInt(response.createdTime, 10);
                    daysAgoLicenseIssued = daysAgoLicenseIssued / 1000 / 60 / 60 / 24;

                    if (daysAgoLicenseIssued <= TRIAL_PERIOD_DAYS) {
                        console.log("On free trial period. " + Math.round(daysAgoLicenseIssued) + " days left." );
                        fetchThreads(result, token, success)
                    } else {
                        console.log("Free trial period expired.");
                        failure()
                    }
                } else {
                    console.log("No license ever issued.");
                }
            });
        });
    });
}

function fetchThreads(result, token, success) {
    let query = getQuery(result, '');
    let uri = encodeURI(GMAIL_API_URL + query);

    return fetch(uri, getRequestOptions(token))
        .then(checkStatus)
        .then(parseJSON)
        .then(function (response) {
            let threads = [];

            if (response.threads) {
                response.threads.forEach(function (thread) {
                    fetch(GMAIL_API_URL + thread.id, getRequestOptions(token))
                        .then(checkStatus)
                        .then(parseJSON)
                        .then(extractSubjectLine)
                        .then(function (response) {
                            threads.push(response);
                            return Utilities.orderByDate(threads);
                        })
                        .then(success)
                });
            } else {
                success([])
            }
        });
}

function getQuery(result, pageToken) {
    let startDate = new Date(`${result.dates.startDate} 00:00:00`) / 1000;
    let endDate = new Date(`${result.dates.endDate} 23:59:59`) / 1000;

    return "?q=in:inbox after:" + startDate + " before:" + endDate + "&pageToken=" + pageToken;
}

function extractSubjectLine(response) {
    let message = response.messages[0];
    let headers = message.payload.headers;

    let subject = headers.find(function (header) {
        return header.name === "Subject";
    });

    let from = headers.find(function (header) {
        return header.name === "From";
    });

    let to = headers.find(function (header) {
        return header.name === "To";
    });

    return {
        id: response.id,
        subject: subject.value,
        snippet: message.snippet,
        createdAt: message.internalDate,
        from: from ? from.value : "",
        to: to ? to.value : ""
    }
}

function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response;
    } else {
        const error = new Error(`HTTP Error ${response.statusText}`);
        error.status = response.statusText;
        error.response = response;
        throw error;
    }
}

function parseJSON(response) {
    return response.json();
}

function getRequestOptions(token) {
    return ({
        headers: {
            Authorization: 'Bearer ' + token,
            'Content-Type': 'application/json'
        },
        'contentType': 'json'
    });
}

const Client = {getThreads};
export default Client;
