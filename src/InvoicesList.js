import PropTypes from "prop-types"
import React, {Component} from 'react';
import Invoice from "./Invoice";

const propTypes = {
    invoices: PropTypes.array.isRequired,
    activeInvoiceId: PropTypes.string,
    onInvoiceClick: PropTypes.func.isRequired
};

class InvoicesList extends Component {
    render() {
        return (
            <div className="mt-5">
                <ul className="list-group list-group-flush text-center">
                    {this.props.invoices.map((invoice) => (
                        <Invoice
                            id={invoice.id}
                            key={invoice.id}
                            title={invoice.title}
                            onInvoiceClick={this.props.onInvoiceClick}
                            activeInvoiceId={this.props.activeInvoiceId}
                        />
                    ))}
                </ul>
            </div>
        )
    }
}

InvoicesList.propTypes = propTypes;

export default InvoicesList;
