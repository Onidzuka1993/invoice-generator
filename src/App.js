/*global chrome*/

import React, {Component} from 'react';
import ThreadsList from "./ThreadsList"
import Client from "./Client"
import ItemsList from "./ItemsList";
import InvoicesList from "./InvoicesList"
import Form from "./Form"
import Utilities from "./Utilities";
import disableScroll from 'disable-scroll';

import { CopyToClipboard } from 'react-copy-to-clipboard';
import { DragDropContext } from 'react-beautiful-dnd';

import 'bootstrap/dist/css/bootstrap.min.css';
import "./css/App.css"

const uuidv4 = require('uuid/v4');

let arrayMove = require('array-move');

class App extends Component {
    state = {
        threads: [],
        items: [],
        invoices: [],
        loading: false,
        activeInvoiceId: null,
        copied: false,
        trialEnded: false
    };

    componentWillMount() {
        this.setState({loading: true})
    }

    componentDidMount() {
        document.addEventListener('copy', (e) => {
            e.clipboardData.setData('text/plain', Utilities.getHighlightedText());
            e.preventDefault();
        });

        if (chrome.storage) {
            Client.getThreads(this.setThreads, this.setTrial);

            this._retrieveInvoices();
        } else {
            console.log('Must be running as chrome extension!')
        }
    }

    handleAddToItems = (id) => {
        let item = this.state.threads.find(function (thread) {
            return thread.id === id
        });

        if (this.state.activeInvoiceId !== null)
            chrome.storage.local.get(['invoices'], (result) => {
                chrome.storage.local.set({invoices: this._addItemToActiveInvoice(result.invoices, item)}, () => {
                    this.setState({items: this.state.items.concat(item)})
                })
            });
        else
            this.setState({items: this.state.items.concat(item)});
    };

    handleUpdateItem = (id, value) => {
        let items = this.state.items.map(item => {
            if (item.id === id) {
                return Object.assign({}, item, {subject: value})
            } else {
                return item
            }
        });

        if (this.state.activeInvoiceId !== null) {
            chrome.storage.local.get(['invoices'], (result) => {
                chrome.storage.local.set({invoices: this._replaceActiveInvoiceItems(result.invoices, items)}, () => {
                    this.setState({items})
                })
            });
        } else {
            this.setState({items})
        }
    };

    handleRemoveItem = (id) => {
        let items = this.state.items.filter(item => item.id !== id);

        if (this.state.activeInvoiceId !== null) {
            chrome.storage.local.get(['invoices'], (result) => {
                chrome.storage.local.set({invoices: this._replaceActiveInvoiceItems(result.invoices, items)}, () => {
                    this.setState({items})
                })
            });
        } else {
            this.setState({items});
        }
    };

    handleRemoveThread = (id) => {
        let threads = this.state.threads.filter(thread => thread.id !== id);
        this.setState({threads})
    };

    handleFormSubmit = (dates) => {
        if (!chrome.runtime.error) {
            this.setState({loading: true});

            this._retrieveThreads(dates);
        } else {
            console.log('Must be running as chrome extension!')
        }
    };

    handleAddToInvoice = () => {
        if (this.state.items.length > 0)
            chrome.storage.local.get(['dates'], (result) => {
                let items = [].concat(this.state.items);

                let title = `${result.dates.startDate} - ${result.dates.endDate}`;
                let invoice = {id: uuidv4(), title, items};

                chrome.storage.local.get(['invoices'], (result) => {
                    let invoices = result.invoices.concat(invoice);

                    chrome.storage.local.set({invoices, activeInvoiceId: null}, () => {
                        this.setState({activeInvoiceId: null, items: [], invoices: invoices})
                    })
                })
            });
    };

    handleShowInvoiceItems = (id) => {
        chrome.storage.local.get(['invoices'], (result) => {
            let invoice = result.invoices.find((invoice) => invoice.id === id);

            chrome.storage.local.set({activeInvoiceId: invoice.id}, () => {
                this.setState({items: invoice.items, activeInvoiceId: invoice.id})
            });
        })
    };

    handleRemoveInvoice = () => {
        chrome.storage.local.get(['activeInvoiceId'], (result) => {
            let activeInvoiceId = result.activeInvoiceId;

            chrome.storage.local.get(['invoices'], (result) => {
                let invoices = result.invoices.filter((invoice) => invoice.id !== activeInvoiceId);

                chrome.storage.local.set({invoices, activeInvoiceId: null}, () => {
                    this.setState({invoices, activeInvoiceId: null, items: []})
                })
            });
        });
    };

    handleNewInvoice = () => {
        chrome.storage.local.set({activeInvoiceId: null}, () => {
            this.setState({activeInvoiceId: null, items: []})
        })
    };

    handleCopy = () => {
        this.setState({copied: true});

        setTimeout(() => {
            this.setState({copied: false});
        }, 1000);
    };

    handleAddItem = () => {
        let item = {id: uuidv4(), subject: ''};

        this.setState({items: this.state.items.concat(item)})
    };

    onDragEnd = (event) => {
        if (event.destination)
            if (this.state.activeInvoiceId !== null) {
                let items = arrayMove(this.state.items, event.source.index, event.destination.index);
                this.setState({items});

                chrome.storage.local.get(['invoices'], (result) => {
                    chrome.storage.local.set({invoices: this._replaceActiveInvoiceItems(result.invoices, items)}, () => {})
                });
            } else {
                this.setState({items: arrayMove(this.state.items, event.source.index, event.destination.index)});
            }

        disableScroll.off();
    };

    onDragStart = () => {
        disableScroll.on();
    };

    showLoader = () => {
        if (this.state.loading)
            return(<i className="fas fa-cog fa-3x loader position-absolute text-center fa-spin"> </i>)
    };

    showAddItemButton = () => {
        if (this.state.items.length > 0)
            return(
                <button
                    className="btn btn-link color-blue font-weight-light add-item"
                    onClick={this.handleAddItem}
                >
                    Add Item
                </button>
            )
    };

    setThreads = (threads) =>
        this.setState({threads, loading: false});

    setTrial = () =>
        this.setState({trialEnded: true, loading: false});

    existsInItems = (id) => {
        return this.state.items.filter(item => item.id === id).length > 0
    };

    itemsToCopy = () => {
        let items = this.state.items.map((item) => (" - " + item.subject));

        return items.join('\n')
    };

    _retrieveThreads = (dates) => {
        chrome.storage.local.set({dates}, () =>
            Client.getThreads(this.setThreads, this.setTrial));
    };

    _retrieveInvoices() {
        chrome.storage.local.get(['invoices'], (result) => {
            if (result.invoices) {
                chrome.storage.local.get(['activeInvoiceId'], (innerResult) => {
                    if (innerResult.activeInvoiceId) {
                        let invoice = result.invoices.find((invoice) => invoice.id === innerResult.activeInvoiceId);
                        this.setState({items: invoice.items, activeInvoiceId: innerResult.activeInvoiceId});
                    }
                });

                this.setState({invoices: result.invoices});
            } else
                chrome.storage.local.set({invoices: []}, () => {})
        });
    }

    _replaceActiveInvoiceItems(invoices, items) {
        return invoices.map((invoice) => {
            if (invoice.id === this.state.activeInvoiceId) {
                invoice.items = items;
                return invoice
            }
            else {
                return invoice
            }
        })
    }

    _addItemToActiveInvoice(invoices, item) {
        return invoices.map((invoice) => {
            if (invoice.id === this.state.activeInvoiceId) {
                invoice.items = invoice.items.concat(item);
                return invoice
            }
            else {
                return invoice
            }
        });
    }

    render() {
        return (
            <div className="container-fluid h-100">
                <div className="row no-gutters h-100">
                    <div className="col-6" style={{zIndex: '99999'}}>
                        <div className="threads-wrapper h-100">
                            <Form
                                onFormSubmit={this.handleFormSubmit}
                            />

                            {this.showLoader()}

                            <ul className="threads-list list-group list-group-flush">
                                <ThreadsList
                                    trialEnded={this.state.trialEnded}
                                    threads={this.state.threads}
                                    onDeleteClick={this.handleRemoveThread}
                                    onAddClick={this.handleAddToItems}
                                    existsInItems={this.existsInItems}
                                />
                            </ul>
                        </div>
                    </div>
                    <div className="col-6">
                        <div className="position-relative h-100">
                            <div className="items py-4 h-100">
                                <div className="row no-gutters h-100">
                                    <div className="col-4 editable-items">
                                        <div className="m-2 pb-4">
                                            <div className="d-flex">
                                                <h5 className="mr-auto ml-3 font-weight-bold color-blue">Invoice</h5>

                                                <button
                                                    className="btn btn-default remove-invoice mr-1 color-red font-weight-light"
                                                    onClick={this.handleRemoveInvoice}
                                                    disabled={this.state.activeInvoiceId === null}
                                                >
                                                    Delete
                                                </button>

                                                <button
                                                    className="btn btn-default new-invoice mr-1 color-blue font-weight-light"
                                                    disabled={this.state.activeInvoiceId === null}
                                                    onClick={this.handleNewInvoice}
                                                >
                                                    New
                                                </button>

                                                <button
                                                    className="btn btn-default save-to-invoice mr-1 color-blue font-weight-light"
                                                    onClick={this.handleAddToInvoice}
                                                    disabled={this.state.items.length === 0 || this.state.activeInvoiceId !== null}
                                                >
                                                    Save
                                                </button>

                                                <CopyToClipboard
                                                    text={this.itemsToCopy()}
                                                    onCopy={this.handleCopy}
                                                >
                                                    <button
                                                        className="btn background-green color-white font-weight-light copy-items shadow-sm"
                                                        disabled={this.state.items.length === 0}
                                                    >
                                                        {this.state.copied ? 'Copied!' : 'Copy'}
                                                    </button>
                                                </CopyToClipboard>
                                            </div>
                                        </div>

                                        <DragDropContext
                                            onDragStart={this.onDragStart}
                                            onDragEnd={this.onDragEnd}
                                        >
                                            <ItemsList
                                                items={this.state.items}
                                                onEditClick={this.handleUpdateItem}
                                                onDeleteClick={this.handleRemoveItem}
                                                onAddClick={this.handleAddItem}
                                            />

                                            <div className="mx-5 my-3">
                                                {this.showAddItemButton()}
                                            </div>
                                        </DragDropContext>
                                    </div>
                                    <div className="col-2">
                                        <InvoicesList
                                            activeInvoiceId={this.state.activeInvoiceId}
                                            invoices={this.state.invoices}
                                            onInvoiceClick={this.handleShowInvoiceItems}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default App;
