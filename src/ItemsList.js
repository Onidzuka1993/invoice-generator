import PropTypes from "prop-types"
import React, {Component} from 'react';
import Item from "./Item";
import {Droppable} from 'react-beautiful-dnd';

const propTypes = {
    items: PropTypes.array.isRequired,
    onEditClick: PropTypes.func.isRequired,
    onDeleteClick: PropTypes.func.isRequired
};

class ItemsList extends Component {
    render() {
        return (
            <div>
                <Droppable
                    droppableId="droppable"
                >
                    {(provided) => (
                        <ul
                            ref={provided.innerRef}
                            className="list-group list-group-flush pl-5"
                        >
                            {this.props.items.map((item, index) => (
                                <Item
                                    provided={provided}
                                    key={item.id}
                                    id={item.id}
                                    index={index}
                                    subject={item.subject}
                                    onEditClick={this.props.onEditClick}
                                    onDeleteClick={this.props.onDeleteClick}
                                />
                            ))}
                        </ul>
                    )}
                </Droppable>
            </div>
        )
    }
}

ItemsList.propTypes = propTypes;

export default ItemsList;
