import React from 'react'
import Invoice from "../Invoice";
import { shallow } from 'enzyme'

const onInvoiceClickMock = jest.fn();

describe('Invoice', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<Invoice
            id="1"
            title="test"
            onInvoiceClick={onInvoiceClickMock}
            activeInvoiceId="1"
        />)
    });

    it('renders invoice', () => {
        expect(wrapper.find('.list-group-item').length).toEqual(1)
    });

    it('adds .active class', () => {
        expect(wrapper.find('.active').length).toEqual(1)
    });

    describe('when item not active', () => {
        it('does not add .active class', () => {
            wrapper.setProps({ activeInvoiceId: '2' });

            expect(wrapper.find('.active').length).toEqual(0)
        })
    })
});
