import React from 'react'
import App from '../App'
import Client from '../Client'
import Form from '../Form'
import ThreadsList from '../ThreadsList'
import ItemsList from "../ItemsList";
import InvoicesList from "../InvoicesList";

import { shallow } from 'enzyme';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { DragDropContext } from 'react-beautiful-dnd';

import chrome from 'sinon-chrome/extensions'

jest.mock('../Client');
jest.mock('uuid/v4', () => () => '0000-0000-0000');
jest.useFakeTimers();

describe('App', () => {
    let wrapper;
    let loader = <i className="fas fa-cog fa-3x loader position-absolute text-center fa-spin"> </i>;

    beforeEach(() => {
        global.chrome = chrome;
        wrapper = shallow(<App/>);
    });

    afterEach(() => {
        chrome.flush();
        delete global.chrome;
        Client.getThreads.mockClear()
    });

    it('inits state', () => {
        expect(wrapper.state()).toEqual({
            threads: [],
            items: [],
            invoices: [],
            loading: true,
            activeInvoiceId: null,
            copied: false,
            trialEnded: false
        })
    });

    it('calls Client.getThreads()', () => {
        expect(Client.getThreads.mock.calls.length).toEqual(1)
    });

    it("renders <Form/>", () => {
        expect(wrapper.find(Form).length).toEqual(1)
    });

    it("renders <ThreadsList/>", () => {
        expect(wrapper.find(ThreadsList).length).toEqual(1)
    });

    it("renders <CopyToClipboard/>", () => {
        expect(wrapper.find(CopyToClipboard).length).toEqual(1)
    });

    it("renders <DragDropContext/>", () => {
        expect(wrapper.find(DragDropContext).length).toEqual(1)
    });

    it("renders <ItemsList/>", () => {
        expect(wrapper.find(ItemsList).length).toEqual(1)
    });

    it("renders <InvoicesList/>", () => {
        expect(wrapper.find(InvoicesList).length).toEqual(1)
    });

    it('fetches invoices from chrome storage', () => {
        expect(chrome.storage.local.get.withArgs(['invoices']).calledOnce).toEqual(true);
    });

    it('fetches active invoice from chrome storage', () => {
        chrome.storage.local.get.withArgs(['invoices']).invokeCallback({invoices: []});

        expect(chrome.storage.local.get.withArgs(['activeInvoiceId']).calledOnce).toEqual(true)
    });

    it('disables create button', () => {
        expect(wrapper.find('.save-to-invoice').props().disabled).toEqual(true)
    });

    it('disables copy button', () => {
        expect(wrapper.find('.copy-items').props().disabled).toEqual(true)
    });

    it('disables new button', () => {
        expect(wrapper.find('.new-invoice').props().disabled).toEqual(true)
    });

    it('does not show delete invoice button', () => {
        expect(wrapper.find('.remove-invoice').props().disabled).toEqual(true)
    });

    it('does not show add item button', () => {
        expect(wrapper.find('.add-item').length).toEqual(0);
    });

    describe('when chrome storage returns invoices', () => {
        let items;
        let invoices;

        beforeEach(() => {
            items = [{id: '11', subject: 'test'}, {id: '12', subject: 'test'}];
            invoices = [{id: '1', items: items}, {id: '2', items: []}];

            chrome.storage.local.get.withArgs(['invoices']).invokeCallback({invoices});
        });

        it('sets state for `invoices`', () => {
            expect(wrapper.state().invoices).toEqual(invoices)
        });

        describe('when chrome storage returns active invoice', () => {
            it('loads items from active invoice', () => {
                chrome.storage.local.get.withArgs(['activeInvoiceId']).invokeCallback({activeInvoiceId: '1'});

                expect(wrapper.state().activeInvoiceId).toEqual('1');
                expect(wrapper.state().items).toEqual(items);
            });
        });

        describe('when no invoices given', () => {
            it('sets invoices state to empty array in chrome storage', () => {
                chrome.storage.local.get.withArgs(['invoices']).invokeCallback({});

                expect(chrome.storage.local.set.withArgs({invoices: []}).calledOnce).toEqual(true);
            });
        });

        describe('when user selects an invoice', () => {
            it('fetches invoices from chrome storage', () => {
                wrapper.find('InvoicesList').props().onInvoiceClick('1');

                expect(chrome.storage.local.get.withArgs(['invoices']).calledTwice).toEqual(true)
            });

            describe('when invoices fetched', () => {
                beforeEach(() => {
                    wrapper.find('InvoicesList').props().onInvoiceClick('1');

                    chrome.storage.local.get.withArgs(['invoices']).invokeCallback({invoices});
                });

                it('marks invoice as active', () => {
                    expect(chrome.storage.local.set.withArgs({activeInvoiceId: '1'}).calledOnce).toEqual(true)
                });

                describe('when the invoice marked', () => {
                    beforeEach(() => {
                        chrome.storage.local.set.withArgs({activeInvoiceId: '1'}).invokeCallback();
                    });

                    it('sets state for `items`', () => {
                        expect(wrapper.state().items).toEqual(invoices[0].items);
                    });

                    it('sets state for `activeInvoiceId`', () => {
                        expect(wrapper.state().activeInvoiceId).toEqual('1');
                    });

                    it('enables copy button', () => {
                        wrapper.update();

                        expect(wrapper.find('.copy-items').props().disabled).toEqual(false)
                    });

                    it('disables save button', () => {
                        wrapper.update();

                        expect(wrapper.find('.save-to-invoice').props().disabled).toEqual(true)
                    });

                    it('it enables delete invoice button', () => {
                        wrapper.update();

                        expect(wrapper.find('.remove-invoice').props().disabled).toEqual(false)
                    });

                    it('it enables new invoice button', () => {
                        wrapper.update();

                        expect(wrapper.find('.new-invoice').props().disabled).toEqual(false)
                    });

                    describe('when user clicks new button', () => {
                        beforeEach(() => {
                            wrapper.find('.new-invoice').simulate('click');
                        });

                        it('sets `current invoice id` to NULL in chrome storage', () => {
                            expect(chrome.storage.local.set.withArgs({activeInvoiceId: null}).calledOnce).toEqual(true)
                        });

                        describe('when `current invoice id` set to NULL', () => {
                            beforeEach(() => {
                                chrome.storage.local.set.withArgs({activeInvoiceId: null}).invokeCallback();
                            });

                            it('sets `activeInvoiceId` state to NULL', () => {
                                expect(wrapper.state().activeInvoiceId).toEqual(null)
                            });

                            it('sets `items` to empty array', () => {
                                expect(wrapper.state().items).toEqual([])
                            });
                        })
                    });

                    describe('when user clicks remove button', () => {
                        beforeEach(() => {
                            wrapper.find('ItemsList').props().onDeleteClick('12');
                        });

                        it('fetches all invoices from chrome storage', () => {
                            expect(chrome.storage.local.get.withArgs(['invoices']).calledThrice).toEqual(true)
                        });

                        describe('when chrome storage returns invoices', () => {
                            let expectedResult = [{id: '1', items: [{id: '11', subject: 'test'}]}, {id: '2', items: []}];

                            beforeEach(() => {
                                chrome.storage.local.get.withArgs(['invoices']).invokeCallback({invoices});
                            });

                            it('removes item from current invoice items', () => {
                                expect(chrome.storage.local.set.withArgs({invoices: expectedResult}).calledOnce).toEqual(true);
                            });

                            describe('when message added to invoice items', () => {
                                beforeEach(() => {
                                    chrome.storage.local.set.withArgs({invoices: expectedResult}).invokeCallback();
                                });

                                it('sets state for `items`', () => {
                                    expect(wrapper.state().items).toEqual([{id: '11', subject: 'test'}])
                                });
                            })
                        });
                    });

                    describe('when user clicks add button', () => {
                        let threads;

                        beforeEach(() => {
                            threads = [{id: '1'}, {id: '2'}];

                            let invocationArgs = Client.getThreads.mock.calls[0];
                            let callback = invocationArgs[0];

                            callback(threads);
                            wrapper.update();

                            wrapper.find('ThreadsList').props().onAddClick('1');
                        });

                        it('fetches all invoices from chrome storage', () => {
                            expect(chrome.storage.local.get.withArgs(['invoices']).calledThrice).toEqual(true)
                        });

                        describe('when chrome storage returns invoices', () => {
                            let expectedItems = [{id: '11', subject: 'test'}, {id: '12', subject: 'test'}, {id: '1'}];
                            let expectedInvoices = [{id: '1', items: expectedItems}, {id: '2', items: []}];

                            beforeEach(() => {
                                chrome.storage.local.get.withArgs(['invoices']).invokeCallback({invoices});
                            });

                            it('adds message to current invoice items', () => {
                                expect(chrome.storage.local.set.withArgs({invoices: expectedInvoices}).calledOnce).toEqual(true);
                            });

                            describe('when message added to invoice items', () => {
                                beforeEach(() => {
                                    chrome.storage.local.set.withArgs({invoices: expectedInvoices}).invokeCallback();
                                });

                                it('sets state for `items`', () => {
                                    expect(wrapper.state().items).toEqual(expectedItems)
                                });
                            })
                        });
                    });

                    describe('when user edits invoice item', () => {
                        beforeEach(() => {
                            wrapper.find('ItemsList').props().onEditClick('11', 'testing');
                        });

                        it('fetches all invoices from chrome storage', () => {
                            expect(chrome.storage.local.get.withArgs(['invoices']).calledThrice).toEqual(true)
                        });

                        describe('when chrome storage returns invoices', () => {
                            let expectedItems = [{id: '11', subject: 'testing'}, {id: '12', subject: 'test'}];
                            let expectedInvoices = [{id: '1', items: expectedItems}, {id: '2', items: []}];

                            beforeEach(() => {
                                chrome.storage.local.get.withArgs(['invoices']).invokeCallback({invoices});
                            });

                            it('saves updated item in current invoice', () => {
                                expect(chrome.storage.local.set.withArgs({invoices: expectedInvoices}).calledOnce).toEqual(true)
                            });

                            describe('when saved in chrome storage', () => {
                                it('sets state for `items`', () => {
                                    chrome.storage.local.set.withArgs({invoices: expectedInvoices}).invokeCallback({invoices: expectedInvoices});

                                    expect(wrapper.state().items).toEqual(expectedItems)
                                });
                            })
                        });
                    });

                    describe('when user changes items order', () => {
                        beforeEach(() => {
                            wrapper.find('DragDropContext').props().onDragEnd({source: {index: 0}, destination: {index: 1}});
                        });

                        it('retrieves invoices from chrome storage', () => {
                            expect(chrome.storage.local.get.withArgs(['invoices']).calledThrice).toEqual(true)
                        });

                        describe('when chrome storage returns invoices', () => {
                            let expectedItems = [{id: '12', subject: 'test'}, {id: '11', subject: 'test'}];
                            let expectedInvoices = [{id: '1', items: expectedItems}, {id: '2', items: []}];

                            beforeEach(() => {
                                chrome.storage.local.get.withArgs(['invoices']).invokeCallback({invoices});
                            });

                            it('reorders invoices and saves to chrome storage', () => {
                                expect(chrome.storage.local.set.withArgs({invoices: expectedInvoices}).calledOnce).toEqual(true)
                            });

                            describe('when saved in chrome storage', () => {
                                it('sets state for `items`', () => {
                                    chrome.storage.local.set.withArgs({invoices: expectedInvoices}).invokeCallback();

                                    expect(wrapper.state().items).toEqual(expectedItems)
                                });
                            })
                        });
                    });
                });
            })
        });

        describe('when user clicks remove invoice', () => {
            beforeEach(() => {
                chrome.storage.local.get.withArgs(['activeInvoiceId']).invokeCallback({activeInvoiceId: '1'});

                wrapper.update();
                wrapper.find('.remove-invoice').simulate('click');
            });

            it('fetches active active invoice from chrome storage', () => {
                expect(chrome.storage.local.get.withArgs(['activeInvoiceId']).calledTwice).toEqual(true)
            });

            describe('when invoice fetched', () => {
                beforeEach(() => {
                    chrome.storage.local.get.withArgs(['activeInvoiceId']).invokeCallback({activeInvoiceId: '1'});
                });

                it('fetches all invoices from chrome storage', () => {
                    expect(chrome.storage.local.get.withArgs(['invoices']).calledTwice).toEqual(true)
                });

                describe('when all invoices fetched', () => {
                    beforeEach(() => {
                        chrome.storage.local.get.withArgs(['invoices']).invokeCallback({invoices});
                    });

                    it('saves updated invoices in chrome storage', () => {
                        expect(chrome.storage.local.set.withArgs({invoices: [{id: '2', items: []}], activeInvoiceId: null}).calledOnce).toEqual(true)
                    });

                    describe('when removed', () => {
                        beforeEach(() => {
                            chrome.storage.local.set.withArgs({invoices: [{id: '2', items: []}], activeInvoiceId: null}).invokeCallback();
                        });

                        it('sets state for `invoices`, `activeInvoiceId`, `items`', () => {
                            expect(wrapper.state().invoices).toEqual([{id: '2', items: []}]);
                            expect(wrapper.state().activeInvoiceId).toEqual(null);
                            expect(wrapper.state().items).toEqual([]);
                        });
                    })
                });
            });
        })
    });

    it('does show loader', () => {
        expect(wrapper.contains(loader)).toEqual(true)
    });

    describe('when API returns threads', () => {
        let threads = [{id: '1'}, {id: '2'}];

        beforeEach(() => {
            let invocationArgs = Client.getThreads.mock.calls[0];
            let callback = invocationArgs[0];

            callback(threads);
            wrapper.update();
        });

        it('sets state from API data', () => {
            expect(wrapper.state().threads).toEqual(threads);
            expect(wrapper.state().loading).toEqual(false);
        });

        it('hides loader loader', () => {
            expect(wrapper.contains(loader)).toEqual(false)
        });

        describe('when user clicks remove button', () => {
            beforeEach(() => {
                wrapper.find('ThreadsList').props().onDeleteClick('1')
            });

            it('removes thread from list', () => {
                expect(wrapper.state().threads).toEqual([{id: '2'}]);
            });
        });

        describe('when user clicks add button', () => {
            let items = [{id: '1'}];

            beforeEach(() => {
                wrapper.find('ThreadsList').props().onAddClick('1');
            });

            it('adds message to items', () => {
                expect(wrapper.state().items).toEqual(items);
            });

            it('enables create button', () => {
                wrapper.update();

                expect(wrapper.find('.save-to-invoice').props().disabled).toEqual(false)
            });
        });
    });

    describe('when free trial ended', () => {
        beforeEach(() => {
            let invocationArgs = Client.getThreads.mock.calls[0];
            let callback = invocationArgs[1];

            callback();
            wrapper.update();
        });

        it('sets state for `trialEnded`', () => {
            expect(wrapper.state().trialEnded).toEqual(true);
        });

        it('sets state for `loading`', () => {
            expect(wrapper.state().loading).toEqual(false);
        })
    });

    describe('when items exist', () => {
        let items = [{id: '1', subject: ''}, {id: '2', subject: ''}, {id: '3', subject: ''}];

        beforeEach(() => {
            wrapper.setState({items});
        });

        it('sets state for `items`', () => {
            expect(wrapper.state().items).toEqual(items)
        });

        it('shows add item button', () => {
            expect(wrapper.find('.add-item').length).toEqual(1)
        });

        describe('when user drags and drops an item', () => {
            it('sets state for items', () => {
                wrapper.find('DragDropContext').props().onDragEnd({source: {index: 0}, destination: {index: 1}});

                expect(wrapper.state().items).toEqual([{id: '2', subject: ''}, {id: '1', subject: ''}, {id: '3', subject: ''}])
            });

            describe('when destination not given', () => {
                it('does not changes order', () => {
                    wrapper.find('DragDropContext').props().onDragEnd({source: {index: 0}});

                    expect(wrapper.state().items).toEqual(items)
                });
            });
        });

        describe('when user edits item', () => {
            let expectedItems = [{id: '1', subject: 'test'}, {id: '2', subject: ''}, {id: '3', subject: ''}];

            beforeEach(() => {
                wrapper.find('ItemsList').props().onEditClick('1', 'test');
            });

            it('updates state for `items`', () => {
                expect(wrapper.state().items).toEqual(expectedItems)
            })
        });

        describe('when user clicks delete item', () => {
            let expectedItems = [{id: '2', subject: ''}, {id: '3', subject: ''}];

            beforeEach(() => {
                wrapper.find('ItemsList').props().onDeleteClick('1')
            });

            it('removes item from items state', () => {
                expect(wrapper.state().items).toEqual(expectedItems);
            });
        });

        describe('when user clicks copy button', () => {
            beforeEach(() => {
                wrapper.find('CopyToClipboard').props().onCopy();
            });

            it('sets state for `copied` to true', () => {
                expect(wrapper.state().copied).toEqual(true)
            });

            it('changes text to `copied`', () => {
                wrapper.update();

                expect(wrapper.find('.copy-items').text()).toEqual('Copied!')
            });

            xit('sets state for `copied` to false after 1 second', () => {
                expect(setTimeout).toHaveBeenCalledTimes(1);
            });
        });

        describe('when user clicks `Add Item` button', () => {
            let item = {id: '0000-0000-0000', subject: ''};
            let expectedItems = items.concat(item);

            beforeEach(() => {
                wrapper.find('.add-item').simulate('click')
            });

            it('adds line item', () => {
                expect(wrapper.find('ItemsList').props().items).toEqual(expectedItems)
            });
        });
    });

    describe('when user clicks save', () => {
        let items;
        let fakeId = '0000-0000-0000';

        beforeEach(() => {
            items = [{id: '1'}, {id: '2'}];

            wrapper.setState({ items });
            wrapper.find('.save-to-invoice').simulate('click');
        });

        it('fetches dates from chrome storage', () => {
            expect(chrome.storage.local.get.withArgs(['dates']).calledOnce).toEqual(true)
        });

        describe('when storage returns dates', () => {
            beforeEach(() => {
                chrome.storage.local.get.withArgs(['dates']).invokeCallback({dates: {startDate: '01/01/2018', endDate: '12/12/2018'}})
            });

            it('fetches all invoices from chrome storage', () => {
                expect(chrome.storage.local.get.withArgs(['invoices']).calledTwice).toEqual(true);
            });

            describe('when chrome returns invoices', () => {
                let expectedResult = [{id: fakeId, title: '01/01/2018 - 12/12/2018', items: [{id: '1'}, {id: '2'}]}];

                beforeEach(() => {
                    chrome.storage.local.get.withArgs(['invoices']).invokeCallback({invoices: []});
                });

                it('adds an invoice to chrome storage', () => {
                    expect(chrome.storage.local.set.withArgs({invoices: expectedResult, activeInvoiceId: null}).calledOnce).toEqual(true)
                });

                describe('when saved in chrome storage', () => {
                    beforeEach(() => {
                        chrome.storage.local.set.withArgs({invoices: expectedResult, activeInvoiceId: null}).invokeCallback();
                    });

                    it('sets state for `items`', () => {
                        expect(wrapper.state().items).toEqual([])
                    });

                    it('sets `activeInvoiceId` to null', () => {
                        expect(wrapper.state().activeInvoiceId).toEqual(null)
                    });

                    it('sets state for `invoices`', () => {
                        expect(wrapper.state().invoices).toEqual(expectedResult)
                    });
                })
            })
        });
    });

    describe('when `Form` invokes `onFormSubmit`', () => {
        let dates = {startDate: '2018/12/12', endDate: '2019/12/12'};

        beforeEach(() => {
            wrapper.find('Form').props().onFormSubmit(dates)
        });

        it('sets `loading` to true', () => {
            expect(wrapper.state().loading).toEqual(true)
        });

        it('saves date range in chrome storage', () => {
            expect(chrome.storage.local.set.withArgs({dates}).calledOnce).toEqual(true)
        });

        describe('when api returns threads', () => {
            let threads = [{}, {}];

            beforeEach(() => {
                chrome.storage.local.set.invokeCallback();

                let invocationArgs = Client.getThreads.mock.calls[1];
                let callback = invocationArgs[0];

                callback(threads);
                wrapper.update()
            });

            it('sets state for `threads`', () => {
                expect(wrapper.state().threads).toEqual(threads)
            });

            it('hides loader loader', () => {
                expect(wrapper.contains(loader)).toEqual(false)
            })
        });

        describe('when trial expired', () => {
            beforeEach(() => {
                chrome.storage.local.set.invokeCallback();

                let invocationArgs = Client.getThreads.mock.calls[1];
                let callback = invocationArgs[1];

                callback();
                wrapper.update()
            });

            it('sets state for `trialEnded`', () => {
                expect(wrapper.state().trialEnded).toEqual(true);
            });

            it('sets state for `loading`', () => {
                expect(wrapper.state().loading).toEqual(false);
            })
        });
    });
});
