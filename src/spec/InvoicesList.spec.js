import React from 'react'
import Invoice from '../Invoice'
import InvoicesList from '../InvoicesList'
import { shallow } from 'enzyme';

const onInvoiceClickMock = jest.fn();

describe('InvoicesList', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<InvoicesList
            invoices={[{id: '1', title: 'test'}, {id: '2', title: 'test'}]}
            onInvoiceClick={onInvoiceClickMock}
            activeInvoiceId="1"
        />)
    });

    it("renders <Invoice /> components", () => {
        expect(wrapper.find(Invoice).length).toEqual(2)
    });
});

