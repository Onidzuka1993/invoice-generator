import React, {Component} from 'react';
import PropTypes from 'prop-types'

const propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    activeInvoiceId: PropTypes.string,
    onInvoiceClick: PropTypes.func.isRequired
};

class Invoice extends Component {
    handleShow = (event) => {
        event.preventDefault();

        this.props.onInvoiceClick(this.props.id)
    };

    isActive = () => {
        return this.props.id === this.props.activeInvoiceId ? 'active' : '';
    };

    render() {
        return(
            <a
                href="#"
                className={"list-group-item list-group-item-action pl-4 py-1 " + this.isActive()}
                onClick={this.handleShow}
            >
                <small className="font-weight-light">{this.props.title}</small>
            </a>
        )
    }
}

Invoice.propTypes = propTypes;

export default Invoice;
