class Utilities {
    static orderByDate(collection) {
        return [].concat(collection).sort(function (a, b) {
            return new Date(Number.parseInt(b.createdAt)) - new Date(Number.parseInt(a.createdAt))
        });
    }

    static getHighlightedText() {
        if (window.getSelection) {
            return window.getSelection().toString();
        } else if (document.selection && document.selection.type !== "Control") {
            return document.selection.createRange().text;
        }
    }
}

export default Utilities
