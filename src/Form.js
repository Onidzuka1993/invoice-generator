/*global chrome*/

import PropTypes from "prop-types"
import React, {Component} from 'react';

import DatePicker from 'react-datepicker';
import moment from 'moment';

import 'react-datepicker/dist/react-datepicker.css';
import './css/Form.css'

const propTypes = {
    onFormSubmit: PropTypes.func.isRequired
};

class Form extends Component {
    state = {
        startDate: '',
        endDate: ''
    };

    componentWillMount() {
        this.setDates((dates) => (this.setState({startDate: dates.startDate, endDate: dates.endDate})));
    };

    handleStartDateChange = (date) => {
        this.setState({startDate: this.formatDate(date)});
    };

    handleEndDateChange = (date) => {
        this.setState({endDate: this.formatDate(date)});
    };

    handleSubmit = (event) => {
        event.preventDefault();

        this.props.onFormSubmit(this.state);
    };

    formatDate = (date) => {
        return moment(date).format('YYYY-MM-DD')
    };

    setDates = (callback) => {
        if (chrome.storage)
            chrome.storage.local.get(['dates'], function (result) {
                callback(result.dates);
            });
        else
            console.log('Must be running as chrome extension')
    };

    render() {
        return (
            <form className="form-inline dates-form" onSubmit={this.handleSubmit}>
                <div className="form-group mx-sm-3 mb-2">
                    <DatePicker
                        selected={moment(this.state.startDate)}
                        onSelect={this.handleStartDateChange}
                        value={this.state.startDate}
                        className="form-control start-date"
                        required
                    />
                </div>
                <div className="form-group mx-sm-2 mb-2 font-weight-bold">
                    to
                </div>
                <div className="form-group mx-sm-3 mb-2">
                    <DatePicker
                        selected={moment(this.state.endDate)}
                        onSelect={this.handleEndDateChange}
                        value={this.state.endDate}
                        className="form-control end-date mr-1"
                        required
                    />
                </div>
                <button
                    type="submit"
                    className="btn mb-2 font-weight-normal px-4 shadow-sm color-blue"
                >
                    Search
                </button>
            </form>
        )
    }
}

Form.propTypes = propTypes;

export default Form;
