export const PRODUCT_LINK = 'https://chrome.google.com/webstore/detail/final-invoice-generator/difljpdonjoeihkacdcbokfiabbcpckc';
export const GMAIL_API_URL = "https://www.googleapis.com/gmail/v1/users/me/threads/";
export const CWS_LICENSE_API_URL = 'https://www.googleapis.com/chromewebstore/v1.1/userlicenses/';
export const TRIAL_PERIOD_DAYS = 14;
