This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

### How to install

1. Install dependencies
```
yarn
```

2. Build the project with
```
yarn build
```

2. Archive build folder and upload it in chrome by going to **chrome://extensions** in your chrome browser

### How to run tests
```
yarn test
```
